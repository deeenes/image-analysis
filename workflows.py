#!/usr/bin/env fiji --headless

# Dénes Türei EMBL 2017
# turei.denes@gmail.com

# Standard Library modules
from __future__ import print_function, division
from future.utils import iteritems
from past.builtins import xrange, range, reduce
import os
import itertools
from collections import defaultdict

# ImageJ Jython modules
from ij import IJ
from ij import ImagePlus
from ij.plugin import ImageCalculator, RGBStackMerge
from ij.plugin.frame import ThresholdAdjuster
from ij.io import FileSaver
from jarray import array


class MultiImageWorkflowBase(object):
    
    def __init__(self,
                 workflow = None,
                 workflow_args = [],
                 workflow_kwargs = {},
                 **kwargs):
        
        self.defaults = {
            'imgType' = 'tif',
            'inDirs' = [
                '/home/denes/documents/autophagy2016/'\
                'all_data_20170427/microscopy/denes_005'
            ],
            'outDir': 'processed',
            'image_filter': lambda i: True
        }
        
        for k, v in iteritems(self.defaults):
            
            setattr(self, k, kwargs[k] if k in kwargs)
        
        if not hasattr(self.image_filter, '__call__'):
            
            self.image_filter = lambda name: self.image_filter in name
        
        self.openImgs = {}
        self._workflow = workflow
        self.workflow_args = workflow_args
        self.workflow_kwargs = workflow_kwargs
        
        self.list_images()
        self.data = defaultdict(lambda: {})
    
    def list_images(self):
        """
        Compiles a list of filenames with all images in the
        specified input directories with the specified ex-
        tension (file type).
        """
        
        self.allImages = list(itertools.chain(*[
                [os.path.join(inDir, 'data', fImg)
                    for fImg in
                    os.listdir(os.path.join(inDir, 'data'))
                    if fImg.endswith(imgType) and
                    self.image_filter(fImg)
                ]
            for inDir in self.inDirs
        ]))
    
    def iterin(self):
        """
        Iterates the input images.
        Yields an `ImagePlus` object in each turn.
        """
        
        for fImg in self.allImages:
            
            img = IJ.openImage(fImg)
            
            yield img
            
            img.close()
    
    def iterout(self, close = False):
        """
        Iterates over output images.
        If one does not exists yet the corresponding input image
        will be copied. If one is not open yet it will be opened
        and added to the `openImgs` dict. Yields an `ImagePlus`
        object in each turn.
        """
        
        for fImg in self.allImages:
            
            fOut = self.get_out_fname(fImg)
            
            if fOut not in self.openImgs:
                
                if not os.path.exists(fOut):
                    
                    shutil.copyfile(fImg, fOut)
                
                self.openImgs[fOut] = IJ.openImage(fOut)
            
            yield self.openImgs[fOut]
            
            if close:
                
                FileSaver(self.openImgs[fOut]).saveAsTiff(fOut)
                del self.openImgs[fOut]
    
    def for_all_in_images(self, callback, **kwargs):
        """
        Calls a method for all input images.
        This should not modify the image as original images
        are not to be overwritten.
        """
        
        for img in self.itersource():
            
            yield callback(img, **kwargs)
    
    def for_all_out_images(self, callback, close = False, **kwargs):
        """
        Applies a method for all images.
        
        :param bool close: Whether to close the images
                           after applying the method.
        """
        
        for img in self.iterout(close = close):
            
            yield callback(img, **kwargs)
    
    def get_out_fname(self, fname):
        """
        For one input image returns a out file name where
        the modified image will be saved.
        """
        
        _fname = os.path.split(fname)[-1]
        _path  = tuple(os.path.split(fname)[:-1])
        _outdir = os.path.join(os.path.join(_path), self.outDir)
        
        if not os.path.isdir(_outdir):
            
            os.mkdir(_outdir)
        
        return os.path.join(_outdir, _fname)
    
    def save_all(self):
        """
        Saves all open images to their default out file.
        """
        
        for fOut, img in iteritems(self.openImgs):
            
            FileSaver(img).saveAsTiff(fOut)
    
    def workflow(self):
        """
        Runs a single image workflow for all images one by one.
        
        :param workflow: A class derived from `SingleImageWorkflowBase`.
        """
        
        for fIn in self.allImages:
            
            fOut = self.get_out_fname(fIn)
            self.workflow_kwargs['infile']  = fnIn
            self.workflow_kwargs['outfile'] = fnOut
            wf = self._workflow(*self.workflow_args,
                                **self.workflow_kwargs)
            wf.run()
            self.data[fOut].update(wf.data)


class SingleImageBase(object):
    
    channels = {
        'green':   2,
        'gray':    4,
        'grey':    4,
        'blue':    3,
        'cyan':    5,
        'red':     1,
        'magenta': 6,
        'yellow':  7
    }
    
    def __init__(self, img = None):
        
        if img is not None:
            self.img = img
    
    def _ij_run(self, method, param = ''):
        
        IJ.run(self.img, method, param)
    
    def _ij_calculate(self, method, other):
        
        ic = ImageCalculator()
        ic.calculate(method, self.img, other)
    
    def green(self):
        
        self._ij_run('Green')
    
    def to32bit(self):
        
        self._ij_run('32-bit')
    
    def to16bit(self):
        
        self._ij_run('16-bit')
    
    def to8bit(self):
        
        self._ij_run('8-bit')
    
    def to_rgb(self, return_new = False):
        
        new = self.img.flatten()
        
        return self.replace_img(new, return_new)
    
    def bitdepth(self):
        
        return self.img.getBitDepth()
    
    def size(self):
        
        return self.img.width, self.img.height
    
    def gaussian_blur(self, sigma = 1.0):
        
        self._ij_run('Gaussian Blur...', 'sigma={}'.format(sigma))
    
    def color_by_wavelength(self, wavelength):
        
        self._ij_run('Set Color By Wavelength',
                     'wavelength={}'.format(wavelength))
    
    def set_min_max(self, mini, maxi):
        
        IJ.setMinAndMax(self.img, mini, maxi)
    
    def sharpen(self):
        
        self._ij_run('Sharpen')
    
    def smooth(self):
        
        self._ij_run('Smooth')
    
    def enhance_contrast(self, saturated = 0.35):
        
        self._ij_run('Enhance Contrast', 'saturated={}'.format(saturated))
    
    def convolve(self, matrix = None, normalize = False):
        
        matrix = matrix or [
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, 24, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
        ]
        
        matrix_str = '\n'.join([' '.join('{}'.format(num)
                                         for num in row)
                                for row in matrix])
        
        norm = ' normalize' if normalize else ''
        
        self._ij_run('Convolve...', 'text1=[%s\n]%s' % (matrix_str, norm))
    
    def unsharp_mask(self, radius = 1, mask = 0.60):
        
        self._ij_run('Unsharp mask...',
                     'radius={} mask={}'.format(radius, mask))
    
    def minimum_filter(self, radius = 2):
        
        self._ij_run('Minimum...', 'radius={}'.format(radius))
    
    def maximum_filter(self, radius = 2):
        
        self._ij_run('Maximum...', 'radius={}'.format(radius))
    
    def subtract_background(self, radius = 50, sliding = False,
                            smoothing = True):
        
        _sliding   = ' sliding' if sliding else ''
        _smoothing = ' disable' if not smoothing else ''
        
        self._ij_run('Subtract background...',
                     'rolling={}{}{}'.format(radius, _sliding, smoothing))
    
    def duplicate(self, title = None):
        
        title = title or 'title=[Duplicate of %s]' % self.img.title
        
        return IJ.run(self.img, 'Duplicate...', title)
    
    def threshold(self, mode = 'B&W', method = 'Default'):
        
        ta = ThresholdAdjuster()
        ta.setMode(mode)
        ta.setMethod(method)
        
        mask = self.duplicate()
        
        
    
    def subtract_32bit(self, other):
        
        self._ij_calculate('Subtract create 32-bit', other)
    
    def get_channel(self, channel):
        
        return (
            self.channels[channel] if channel in self.channels
            else channel if type(channel) is int
            else int(channel[1:]) if channel[1:].isdigit()
            else 1
        )
    
    def merge_channels(self, images, channels, return_new = False):
        """
        Merges channels.
        
        :param list others: List images; either single image instances
                            having `img` attribute or `ImagePlus` objects.
        :param list channels: List of channels: names, labels or numbers,
                              e.g. `'green'` or `'c2'` or `2`.
        :param bool return_new: Return the new image as ImagePlus object and
                                keep the old ones, or replace the `img`
                                attribute of the current object.
        """
        
        images  = [i if type(i) is ImagePlus else i.img for i in images]
        images  = dict((self.get_channel(c), i)
                       for i, c in zip(images, channels))
        images  = [images[c] if c in images else None for c in xrange(1,8)]
        images  = array(images, ImagePlus)
        
        sm = RGBStackMerge()
        
        new = sm.mergeChannels(images, return_new)
        
        return self.replace_img(new, return_new)
        
    def replace_img(self, new, return_new = False):
        """
        Replaces the default image and the output image
        with the new one supplied.
        """
        
        if return_new:
            
            return new
        
        self.img = new
        
        if hasattr(self, 'imgout'):
            
            self.imgout = new


class SingleImageWorkflowBase(SingleImageBase):
    
    def __init__(self, infile = None, outfile = None,
                 previous = None, **kwargs):
        
        if (infile is None and outfile is None) and previous is None:
            
            raise TypeError('%s.__init__(): Please provide either infile'\
                'and outfile or previous instance.' % self.__class__.__name__)
        
        self.closeatend = True
        
        if previous is not None:
            
            self.infile = previous.infile
            self.outfile = previous.outfile
            self.data    = previous.data
            self.others  = previous.others
            self.param   = previous.param
            
        else:
            
            self.infile  = infile
            self.outfile = outfile
            self.data    = {}
            self.others  = {}
            self.param   = {}
        
        self.param.update(kwargs)
        
        for k, v in iteritems(kwargs):
            
            setattr(self, k, v)
        
        SingleImageBase.__init__(self)
    
    def run(self):
        """
        Wrapper around `_run()`.
        """
        
        if not hasattr(self, 'imgout'):
            # opens if necessary; in case of continuation
            # we already have an image open
            
            self.openout()
        
        self._run()
        
        if self.closeatend:
            # if we want to continue, image should not be
            # closed
            
            self.closeout()
    
    def _run(self):
        """
        This method every derived class should override.
        """
        
        pass
    
    def openin(self):
        """
        Opens the input file providing an `ImagePlus` object.
        """
        
        self.imgin = IJ.imageOpen(self.infile)
    
    def openout(self):
        """
        Opens the output file providing an `ImagePlus` object.
        """
        
        if not os.path.exists(self.outfile):
            
            self.shutil.copyfile(self.infile, self.outfile)
        
        self.imgout = IJ.imageOpen(self.outfile)
        self.img = self.imgout
    
    def save(self):
        """
        Saves the output image.
        """
        
        FileSaver(self.imgout).saveAsTiff(self.outfile)
    
    def closeout(self):
        """
        Closes the output image.
        """
        
        if hasattr(self, 'imgout'):
            
            self.save()
            self.imgout.close()
            delattr(self, 'imgout')
            delattr(self, 'img')
    
    def closein(self):
        """
        Closes the input image.
        """
        
        if hasattr(self, 'imgin'):
            
            self.imgin.close()
    
    def saveother(self, other, label):
        """
        Saves an other image with same name plus label
        into the same directory.
        Useful for preserving masks or intermediate processed stages.
        """
        
        _fname = os.path.split(self.outfile)[-1]
        _path  = os.path.join(os.path.split(self.outfile)[:-1])
        _fname, _ext   = os.path.splitext(_fname)
        _fname = '{}__{}{}'.format(_fname, label, _ext)
        _fname = os.path.join(_path, _fname)
        
        FileSaver(other).saveAsTiff(_fname)
    
    def get_corresponding(self, label1, label2):
        """
        Opens an other image with its name only different in their label.
        E.g. if one has the fluorescent images with names ending `GFP`,
        and also corresponding transmission images `TRS`, juts pass
        `label1 = 'GFP', label2 = 'TRS'`.
        Find the images opened in under the `others` attribute by
        the key `label2`.
        """
        
        name = self.infile.replace(label1, label2)
        
        if os.path.exists(name):
            
            self.others[label2] = SingleImageBase(IJ.imageOpen(name))
            self._lastkey = label2
            
        else:
            
            raise FileNotFoundError('No such file: %s' % name)
    
    def get_other(self, other = None, label1 = None, label2 = None):
        
        if other is None:
            
            self.get_corresponding(self.label1, self.label2)
            
        elif hasattr(other, 'capitalize'):
            
            if os.path.exists(other):
                
                self.others[other] = SingleImageBase(IJ.openImage(other))
                self._lastkey = other
                
            else:
                
                _path  = os.path.join(os.path.split(self.infile)[:-1])
                _other = os.path.join(_path, other)
                
                if os.path.exists(_other):
                    
                    self.others[other] = SingleImageBase(IJ.openImage(_other))
                    self._lastkey = other
            
        elif type(other) is ImagePlus:
            
            self.others[other.title] = SingleImageBase(other)
            self._lastkey = other.title
        
        elif type(other) is SingleImageBase:
            
            self.others[other.img.title] = other
            self._lastkey = other.img.title
            
        else:
            
            raise TypeError('Could not understand `%s`' % other.__str__())


class Overlay(SingleImageWorkflowBase):
    
    def __init__(self,
                 channel1, channel2,
                 infile = None, outfile = None,
                 previous = None, label1 = None, label2 = None,
                 overlay = None, **kwargs):
        
        self.label1  = label1
        self.label2  = label2
        self.overlay = overlay
        
        SingleImageWorkflowBase.__init__(self, infile, outfile,
                                         previous, **kwargs)
    
    def _run(self):
        
        self.to8bit()
        self.get_other(other = self.overlay,
                       label1 = self.label1,
                       label2 = self.label2)
        _overlay = self.others[self._lastkey]
        _overlay.to8bit()
        self.merge_channels([self.img, _overlay.img],
                            [channel1, channel2])
        self.to_rgb()
        self.save()


class MultiImageWorkflow(MultiImageWorkflowBase):
    
    def __init__(self, **kwargs):
        
        MultiImageWorkflowBase.__init__(self, **kwargs)
    
    def get_min_max(self):
        """
        Obtains the minimum and maximum values across all images.
        """
        
        def _min_max(img):
            
            stats = img.getStatistics()
            return stats.min, stats.max
        
        _min = None
        _max = None
        
        for _mini, _maxi in self.for_all_out_images(_min_max):
            
            _min = min(_min, _mini)
            _max = max(_max, _maxi)
        
        self._min = _min
        self._max = _max
